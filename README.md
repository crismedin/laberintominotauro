![banner](https://image.shutterstock.com/image-vector/ancient-greece-banner-hunting-minotaur-260nw-1818615179.jpg)
# Titulo del proyecto: Laberinto Minotauro
### Resolver laberinto minatauro

## Indice
1. [Caracteristicas](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologias](#tecnologias)
4. [IDE](#ide)
5. [Instalacion](#instalacion)
6. [Demo](#demo)
7. [Autores](#autores)
8. [Institucion Academica](#institucion-academica)
9. [Referencias](#referencias)

#### Caracteristicas
    . proyecto hecho en java el cual lee un documento de texto que representa un laberinto con un minotauro
    . solución del problema con metodo recursivo
    
#### Contenido del proyecto

| Archivos  | Descripción                                                                                                                                                                                                                                                           |
|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Proyecto  https://gitlab.com/crismedin/laberintominotauro  | un ejercicio hecho en java el cual resuelve un laberinto representado  con un archivo de texto, el programa encuentra la salida mas rapida del laberinto no sin antes pasar por donde esta el minotauro el cual se representa con el numero 1000 dentro del laberinto |
| [Laberinto](src/Negocio/Laberinto.java)  🐴 | Esta clase es la que contiene la logica que nos ayuda a resolver  el laberinto encontrando el camino mas rapido en el cual se pasa por donde el minotauro y se llega a la salida o indica que no hay manera de resolver el laberinto si es el caso                    |
| [Matriz](src/Vista/matriz.java)  🔟🔟  | Esta clase es la encargada de convertir el archivo de texto a una matriz para que se pueda resolver el ejercicio                                                                                                                                                      |
#### Tecnologias

[Java](https://www.java.com/es/) el cual es un lenguaje de programación y una plataforma informática que fue comercializada por primera vez en 1995 por Sun Microsystems. Hay muchas aplicaciones y sitios web que no funcionarán, probablemente, a menos que tengan Java instalado y cada día se crean más. Java es rápido, seguro y fiable. Desde ordenadores portátiles hasta centros de datos, desde consolas para juegos hasta computadoras avanzadas, desde teléfonos móviles hasta Internet, Java está en todas partes, si es ejecutado en una plataforma no tiene que ser recompilado para correr en otra. Java es, a partir de 2012, uno de los lenguajes de programación más populares en uso, particularmente para aplicaciones de cliente-servidor de web, con unos diez millones de usuarios reportado

[NetBeans](https://netbeans.apache.org/) NetBeans es un entorno de desarrollo integrado libre, hecho principalmente para el lenguaje de programación Java.

[![Java](https://img.shields.io/badge/-Java-green)](https://www.java.com/es/)
[![NetBeans](https://img.shields.io/badge/-NetBeans-orange)](https://netbeans.apache.org/)


#### IDE

.Se uso la IDE [NetBeans](https://netbeans.apache.org/download/index.html) la cual nos porporciona un entorno de desarrollo de software  hecho principalmente para el lenguaje de programación Java. Existe además un número importante de módulos para extenderlo. NetBeans IDE​ es un producto libre y gratuito sin restricciones de uso.


![netbeans](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Apache_NetBeans_Logo.svg/245px-Apache_NetBeans_Logo.svg.png)


#### Instalacion
Descargamos Netbeans en caso de que no lo tengamos [ir](https://netbeans.apache.org/download/index.html)
    
    .despues decargamos el codigo fuente del proyecto
    .una vez hecho eso descomprimimos el proyecto y lo guardamos en un lugar de nuestra preferencia
    .abrimos el netbeans y en la parte de file, sealamos la tercera opcion de abrir proyecto
    .se nos abrira una ventana donde seleccionaremos el proyecto donde lo hallamos descargado

#### Demo
![demo](img/laberinto.png)

#### Autores
Realiado por [Johan Alberto león Serrano](<johanalbertols@ufps.edu.co>)
y por [Cristian Manuel Medina Perez](<critianmanuelmp@ufps.edu.co>)

#### Institucion Academica
Proyecto Realizado en la materia de programación web de ingenieria en sistemas [aqui](https://ww2.ufps.edu.co/oferta-academica/ingenieria-de-sistemas) en la  Universidad Francisco de Paula Santander [aqui](https://ww2.ufps.edu.co/)

#### Referencias

###### The Apache Software Foundation. (2017-2020). NetBeans: Development Environment, Tooling Platform and Application Framework
