/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author Cristian
 */
public class Laberinto {

    private short laberinto[][];
    String s;
    
    public Laberinto(String url){
        ArchivoLeerURL file=new ArchivoLeerURL(url);
        Object v[]=file.leerArchivo();
        this.laberinto = this.normalizado(v);
        
    }
    
    private short[][] normalizado(Object v[]){
        short matriz[][] = new short[v.length][];
        
        for (int i = 0; i < v.length; i++) {
            String linea[] = ((String)v[i]).split(";");
            matriz[i] = new short[linea.length];
            
            for (int j = 0; j < linea.length; j++) {
                matriz[i][j] = Short.parseShort(linea[j]);
            }
        }
        return matriz;
    }
    
    public String getCamino(int filaTeseo, int colTeseo, int filaMin, int colMin, int filaSalida, int colSalida)throws Exception{
        
        String s = this.getCamino(filaTeseo, colTeseo, filaMin, colMin, this.laberinto, false, "", new String[1]);
        if(s==null)
            throw new Exception("No se puede resolver");
        return masCorto(s);
    }
    
    private String getCamino(int filaTeseo, int colTeseo, int filaMin, int colMin,short [][]l, boolean matoMin, String movs, String caminos[]){
        
        if((filaTeseo<0 || filaTeseo==l.length)  || (colTeseo<0 || colTeseo==l[filaTeseo].length))
                return "";
        else if(l[filaTeseo][colTeseo] == -1)
                return "";
        else{
            if((filaMin-filaTeseo == 0) && (colTeseo-colMin==0)){
                matoMin = true;
            }
            if(l[filaTeseo][colTeseo] == 1000){
                if(matoMin){
                    caminos[0] +=movs+";";
                }
            }
            else{
                l[filaTeseo][colTeseo] = -1;
                
                movs = movs +"("+filaTeseo+","+colTeseo+")";
                getCamino(filaTeseo+1, colTeseo, filaMin, colMin, l, matoMin, movs, caminos);
                getCamino(filaTeseo, colTeseo+1, filaMin, colMin, l, matoMin, movs, caminos);
                getCamino(filaTeseo-1, colTeseo, filaMin, colMin, l, matoMin, movs, caminos);
                getCamino(filaTeseo, colTeseo-1, filaMin, colMin, l, matoMin, movs,caminos);
                
                l[filaTeseo][colTeseo] = 0;
            }
        }
        return caminos[0];
    }
    
    public short[][] caminoMatriz(String camino,int filaMin, int colMin){
        
        short m[][] = this.laberinto;
        if(camino!=null){
            String ca = camino.replaceAll(",", "").replaceAll("[()]", "");
            m[Character.getNumericValue(ca.charAt(0))][Character.getNumericValue(ca.charAt(1))] = 9;
            
            for (int i = 2; i < (ca.length()); i+=2) {
                
                int fila = Character.getNumericValue(ca.charAt(i));
                int col = Character.getNumericValue(ca.charAt(i+1));
                if(fila == filaMin && col == colMin)
                    m[fila][col] = -100;
                else
                    m[fila][col] = 1;
            }
        
        }
        return m;
    }
    
    private String masCorto(String camino){
        String corto ="";
        if(camino!=null){
        String caminos[] = camino.split(";");
            corto = caminos[0];
            for (int i = 0; i < caminos.length; i++) {
                if(corto.length() > caminos[i].length())
                    corto = caminos[i];
            }
        }
        return corto;
    }
 
    public short[][] getLaberinto() {
        return laberinto;
    }

    public void setLaberinto(short[][] laberinto) {
        this.laberinto = laberinto;
    }
    
    
    }
    
