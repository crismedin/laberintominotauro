/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.Laberinto;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author Cristian
 */
public class Main {
    public static void main(String[] args)throws Exception {
        
        String url="http://madarme.co/persistencia/laberintoTeseo2.txt";
//         String url = "http://madarme.co/persistencia/laberintoTeseo3.txt";
//         String url="http://madarme.co/persistencia/laberintoTeseo.txt?fbclid=IwAR1BLmipvL5MCkz0cdaEOJ4GGn6kU6Q_WXmgXax4Yox2mou2G-M_2X4xfZU";
        Laberinto m = new Laberinto(url);
        matriz ma = new matriz();
        String sol = m.getCamino(8, 1,4,0,8,4);
        System.out.println("Camino de Theseus : "+sol);
         
        ma.mostrarMatriz(m.caminoMatriz(sol, 4, 0));
        ma.setVisible(true);
        
    }
}
