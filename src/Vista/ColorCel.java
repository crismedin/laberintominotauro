/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.awt.Component;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Cristian
 */
public class ColorCel extends DefaultTableCellRenderer{
    
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
//   
     JLabel celda = (JLabel)super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        
        short valor = (short)value;
        if(valor == 1)
            celda.setBackground(Color.GREEN);
        else if(valor == -100 || valor == 9 || valor == 1000)
            celda.setBackground(Color.ORANGE);
        else
            celda.setBackground(Color.WHITE);
        return celda;
    }

}
